<?php include('header.php'); include('nav.php'); ?>
<main id="main">
	<div class="row">
	<div class="col-md-6">
		<section id="testimonials" class="testimonials">
			<div class="container" data-aos="fade-up">
				<div class="owl-carousel testimonials-carousel">
					<div class="testimonial-item">
						<img src="assets/img/slide/1.jpg" alt="">
					</div>
					<div class="testimonial-item">
						<img src="assets/img/slide/2.jpg"  alt="">
					</div>
					<div class="testimonial-item">
						<img src="assets/img/slide/3.jpg" alt="">
					</div>
				</div>
			</div>
	    </section>
	</div>
	<div class="col-md-6">
		<section id="resume" class="resume" >
    	<div class="container" data-aos="fade-up">
    		<div class="row">
				<div class="col-md-12">
					<h3 >Original Version of Some product name</h3>
				</div>
				<div class="col-md-12">
					<p><span>US $1299</span> /per kg</p>
				</div>
				<div class="col-md-12">
					<h3>Description</h3>
    				<p>Here goes description consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco </p>
    				<h4>Model#</h4>
				  	<p>12345611</p>
				  	<h4>Color#</h4>
				  	<p>Black and white</p>
				  	<h4>Delivery#</h4>
				  	<p>Russia, USA, and Europe</p>
				</div>
				<div class="col-md-12">
					<p>Quantity: </p>
					<select class="form-control form-control-sm" style="width:70px;">
						<option> 1 </option>
						<option> 2 </option>
						<option> 3 </option>
					</select>
		  		</div>
		  		<div class="col-md-12">
		  			<p>Size: </p>
			  
				  	<label class="form-check form-check-inline">
					  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
					  <span class="form-check-label">SM</span>
					</label>
					<label class="form-check form-check-inline">
					  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
					  <span class="form-check-label">MD</span>
					</label>
					<label class="form-check form-check-inline">
					  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
					  <span class="form-check-label">XXL</span>
					</label>
		  		</div>
		  		<div class="col-md-12">
		  			<a href="#" class="btn btn-lg btn-primary text-uppercase"> Buy now </a>
		  			<a href="#" class="btn btn-lg btn-outline-primary text-uppercase"> <i class="icofont-shopping-cart"></i> Add to cart </a>
		  			<a href="#" class="btn btn-lg btn-outline-warning text-uppercase"> Wishlist </a>
		  		</div>
			</div>
		</div>
    	</section>
	</div>
	</div>

<div class="container">
<ul class="nav nav-tabs" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" href="#profile" role="tab" data-toggle="tab">Product Description</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#buzz" role="tab" data-toggle="tab">Comentar</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#references" role="tab" data-toggle="tab">Gallery</a>
  </li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
  <div role="tabpanel" class="tab-pane fade in active" id="profile"><p>Product ini adalah product terbaik yang dibuat menggunakan bahan terbaik sehingga penting dan bagus untuk di miliki.  Product ini adalah product terbaik yang dibuat menggunakan bahan terbaik sehingga penting dan bagus untuk di miliki.</p></div>
  <div role="tabpanel" class="tab-pane fade" id="buzz">Ini Halaman Profile</div>
  <div role="tabpanel" class="tab-pane fade" id="references">Ini Halaman Setting</div>
</div>
<div>
    <p>Product ini adalah product terbaik yang dibuat menggunakan bahan terbaik sehingga penting dan bagus untuk di miliki </p>
</div>
</div>

</main>




<?php include('footer.php');?>

<?php include('footer_end.php');?>