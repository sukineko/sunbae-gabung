<?php include('header.php'); include('nav.php'); ?>
<main id="main">
	<section id="resume" class="resume" style="background-color:#e7e6e1; ">
    	<div class="container" data-aos="fade-up">
    		<div class="section-title">
    			<div class="row">
	    			<div class="col-md-4">
	    				<img src="assets/img/maskot/WorkPanda.png" height="150px" width="150px"/>
	    			</div>
	    			<div class="col-md-8" style="margin-top:50px; margin-left:-10%;">
	    				<h1 class="jumbotron-heading" style="display:inline;font-weight: bold;">MY PERSONAL SPACE</h1>
	    			</div>
	    		</div>
        	</div>
        </div>
    </section>
    <section id="services" class="services" style="margin-top:-5%;">
    	<div class="container" data-aos="fade-up">
    		<div class="row">
		  <div class="col-md-3">
			<div class="card mb-4 box-shadow">
			  <div class="card-body" >
			    <div class="row">
			        <a style="color:black;" href="orderhistory.php">
    				<div class="col-md-4">
    				    <i class="icofont-listine-dots"></i>
    				</div>
    				<div class="col-md-8" style="font-size:10pt;">
    				    
    				    <b>O R D E R</b><br/></a>
    				    
    				    HISTORY OF ALL THE ITEMS PURCHASED FROM OUR WEBSITE SHOP.
    				</div>
    				
				</div>
			  </div>
			</div>
		  </div>
		  <div class="col-md-3">
			<div class="card mb-4 box-shadow">
			  <div class="card-body" >
			    <div class="row">
			        <a style="color:black;" href="request.php">
    				<div class="col-md-4">
    				   <i class="icofont-plus-square"></i>
    				</div>
    				<div class="col-md-8" style="font-size:10pt;">
    				    <b>R E Q U E S T S</b><br/></a>
    				    HISTORY OF ALL THE ITEMS WE PURCHASED FOR YOUR UPON YOUR REQUEST
    				</div>
				</div>
			  </div>
			</div>
		  </div>
		  <div class="col-md-3">
			<div class="card mb-4 box-shadow">
			  <div class="card-body" >
			    <div class="row">
			        <a style="color:black;" href="foitem.php">
    				<div class="col-md-4">
    				    <i class="icofont-ui-next"></i>
    				</div>
    				<div class="col-md-8" style="font-size:10pt;">
    				    <b>FOWARDING ITEMS</b><br/></a>
    				    HISTORY OF ALL THE ITEMS PURCHASED FROM OUR WEBSITE SHOP.
    				</div>
				</div>
			  </div>
			</div>
		  </div>
		  <div class="col-md-3">
			<div class="card mb-4 box-shadow">
			  <div class="card-body" >
			    <div class="row">
			        <a style="color:black;" href="package.php">
    				<div class="col-md-4">
    				     <i class="icofont-cube"></i>
    				</div>
    				<div class="col-md-8" style="font-size:10pt;">
    				    <b>PACKAGES</b><br/></a>
    				    HISTORY OF ALL THE ITEMS PURCHASED FROM OUR WEBSITE SHOP.
    				</div>
				</div>
			  </div>
			</div>
		  </div>
		  <div class="col-md-3">
			<div class="card mb-4 box-shadow">
			  <div class="card-body" >
			    <div class="row">
			        <a style="color:black;" href="wishlist.php">
    				<div class="col-md-4">
    				     <i class="icofont-ui-love"></i>
    				</div>
    				<div class="col-md-8" style="font-size:10pt;">
    				    <b>WISHLIST</b><br/></a>
    				    HISTORY OF ALL THE ITEMS PURCHASED FROM OUR WEBSITE SHOP.
    				</div>
				</div>
			  </div>
			</div>
		  </div><div class="col-md-3">
			<div class="card mb-4 box-shadow">
			  <div class="card-body" >
			    <div class="row">
			        <a style="color:black;" href="address.php">
    				<div class="col-md-4">
    				     <i class="icofont-home"></i>
    				</div>
    				<div class="col-md-8" style="font-size:10pt;">
    				    <b>ADDRESS BOOK</b><br/></a>
    				    HISTORY OF ALL THE ITEMS PURCHASED FROM OUR WEBSITE SHOP.
    				</div>
				</div>
			  </div>
			</div>
		  </div>
		  <div class="col-md-3">
			<div class="card mb-4 box-shadow">
			  <div class="card-body" >
			    <div class="row">
			        <a style="color:black;" href="myinfo.php">
    				<div class="col-md-4">
    				     <i class="icofont-ui-user"></i> 
    				</div>
    				<div class="col-md-8" style="font-size:10pt;">
    				    <b>INFORMATION</b><br/></a>
    				    HISTORY OF ALL THE ITEMS PURCHASED FROM OUR WEBSITE SHOP.
    				</div>
				</div>
			  </div>
			</div>
		  </div><div class="col-md-3">
			<div class="card mb-4 box-shadow">
			  <div class="card-body" >
			    <div class="row">
    				<div class="col-md-4">
    				     <i class="icofont-info-circle"></i>
    				</div>
    				<div class="col-md-8" style="font-size:10pt;">
    				    <b>CUSTOMER CENTER</b><br/>
    				    HISTORY OF ALL THE ITEMS PURCHASED FROM OUR WEBSITE SHOP.
    				</div>
				</div>
			  </div>
			</div>
		  </div>
		</div>
    	</div>
    </section>
</main>
<?php include('footer.php');?>
<?php include('footer_end.php');?>