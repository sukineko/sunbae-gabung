<?php include('header.php'); include('nav.php'); ?>
<main id="main">
	<section id="testimonials" class="testimonials">
		<div class="container" data-aos="fade-up">
			<div class="owl-carousel testimonials-carousel">
				<div class="testimonial-item">
					<img src="assets/img/slide/1.jpg" alt="">
					<h3>BUYING SERVICES</h3>
					<h4>sunbae shop</h4>
				</div>
				<div class="testimonial-item">
					<img src="assets/img/slide/2.jpg"  alt="">
					<h3>FREE WAREHOUSE</h3>
					<h4>sunbae shop</h4>
				</div>
				<div class="testimonial-item">
					<img src="assets/img/slide/3.jpg" alt="">
					<h3>PACKAGE CONSOLIDATION</h3>
					<h4>sunbae shop</h4>
				</div>
			</div>
		</div>
    </section>

    <section id="services" class="services" style="margin-top:-5%;">
    	<div class="container" data-aos="fade-up">
    		<div class="row" style="background-color:#ccebe6;">
    			<div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
    				<div class="icon-box iconbox-blue" style="background-color:#ccebe6;">
    					<h4><a href="">BUYING SERVICES</a></h4>
    					<p>This is a wider card with supporting text below as a .</p>
    					<p><small>Last updated 3 mins ago</small></p>
    				</div>
    			</div>
    			<div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
    				<div class="icon-box iconbox-blue" style="background-color:#ccebe6;">
    					<h4><a href="">FREE WAREHOUSE</a></h4>
    					<p>This is a wider card with supporting text below as a .</p>
    					<p><small>Last updated 3 mins ago</small></p>
    				</div>
    			</div>
    			<div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
    				<div class="icon-box iconbox-blue" style="background-color:#ccebe6;">
    					<h4><a href="">PACKAGE CONSOLIDATION</a></h4>
    					<p>This is a wider card with supporting text below as a .</p>
    					<p><small>Last updated 3 mins ago</small></p>
    				</div>
    			</div>
    		</div>
    	</div>
    </section>

    <section id="resume" class="resume" style="margin-top:-2%;background-image: linear-gradient(to right, #e6b3cc , #f2d9e6);">
    	<div class="container" data-aos="fade-up">
    		<div class="section-title">
          		<p>Something short and leading about the collection below—its contents, the creator, etc. Make it short and sweet, but not too short so folks don't simply skip over it entirely.</p>
          		<p>
				</p>
        	</div>
        </div>
    </section>

    <section id="services" class="services">
    	<div class="container" data-aos="fade-up">
    		<div class="row">
    			<div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
    				<div class="card mb-4">
    					<img class="card-img-top" src="assets/img/ff.jpg" alt="Card image cap">
    					<div class="card-body">
    						<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
    						<div class="d-flex justify-content-between align-items-center">
    							<div class="btn-group">
    								<button type="button" class="btn btn-sm btn-outline-secondary">View</button>
    							</div>
    							<small class="text-muted">9 mins</small>
    						</div>
    					</div>
    				</div>
    			</div>
    			<div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
    				<div class="card mb-4">
    					<img class="card-img-top" src="assets/img/ff.jpg" alt="Card image cap">
    					<div class="card-body">
    						<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
    						<div class="d-flex justify-content-between align-items-center">
    							<div class="btn-group">
    								<button type="button" class="btn btn-sm btn-outline-secondary">View</button>
    							</div>
    							<small class="text-muted">9 mins</small>
    						</div>
    					</div>
    				</div>
    			</div>
    			<div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
    				<div class="card mb-4">
    					<img class="card-img-top" src="assets/img/ff.jpg" alt="Card image cap">
    					<div class="card-body">
    						<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
    						<div class="d-flex justify-content-between align-items-center">
    							<div class="btn-group">
    								<button type="button" class="btn btn-sm btn-outline-secondary">View</button>
    							</div>
    							<small class="text-muted">9 mins</small>
    						</div>
    					</div>
    				</div>
    			</div>
    			<div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
    				<div class="card mb-4">
    					<img class="card-img-top" src="assets/img/ff.jpg" alt="Card image cap">
    					<div class="card-body">
    						<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
    						<div class="d-flex justify-content-between align-items-center">
    							<div class="btn-group">
    								<button type="button" class="btn btn-sm btn-outline-secondary">View</button>
    							</div>
    							<small class="text-muted">9 mins</small>
    						</div>
    					</div>
    				</div>
    			</div>
    			<div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
    				<div class="card mb-4">
    					<img class="card-img-top" src="assets/img/ff.jpg" alt="Card image cap">
    					<div class="card-body">
    						<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
    						<div class="d-flex justify-content-between align-items-center">
    							<div class="btn-group">
    								<button type="button" class="btn btn-sm btn-outline-secondary">View</button>
    							</div>
    							<small class="text-muted">9 mins</small>
    						</div>
    					</div>
    				</div>
    			</div>
    			<div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
    				<div class="card mb-4">
    					<img class="card-img-top" src="assets/img/ff.jpg" alt="Card image cap">
    					<div class="card-body">
    						<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
    						<div class="d-flex justify-content-between align-items-center">
    							<div class="btn-group">
    								<button type="button" class="btn btn-sm btn-outline-secondary">View</button>
    							</div>
    							<small class="text-muted">9 mins</small>
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>
    		<?php include('paging.php');?>
    	</div>
    </section>
</main>
<?php include('footer.php');?>

<?php include('footer_end.php');?>