<?php include('header.php'); include('nav.php'); ?>
<main id="main">
	<section id="resume" class="resume" style="background-color:#adce74; ">
    	<div class="container" data-aos="fade-up">
    		<div class="section-title" style="margin-top:50px;">
    			<div class="row">
	    			<div class="col-md-4">
	    				<img src="assets/img/maskot/WorkPanda.png" height="150px" width="150px"/>
	    			</div>
	    			<div class="col-md-8" style="margin-top:50px; margin-left:-10%;">
	    				<h1 class="jumbotron-heading" style="display:inline;font-weight: bold;">MY INFORMATIONS</h1>
	    			</div>
	    		</div>
        	</div>
        </div>
    </section>
    <section id="services" class="services">
    	<div class="container" data-aos="fade-up">
    		
            <div class="row">
    			<div class="col-md-3">
    				<div class="col-12">
    					<?php include('sidebar.php');?>
			        </div>
    			</div>
    			<div class="col-md-9">
    				<div class="row">
			          <div class="col-md-12">
			              <p style="font-weight: bold;">Personal Details</p>
			              <hr style="width: 100%; color: #ffabe1; height: 1px; background-color:#adce74;" />
			          </div>
			      </div>
			      <div class="form-group row">
			          <label for="inputEmail3" class="col-sm-2 col-form-label">Username</label>
			          <div class="col-sm-10">
			              <input type="text" class="form-control" id="inputEmail3" placeholder="Username">
			            </div>
			     </div>
			      <div class="form-group row">
			          <label for="inputEmail3" class="col-sm-2 col-form-label">Password</label>
			          <div class="col-sm-3">
			              <input type="password" class="form-control" id="inputEmail3" placeholder="Password">
			            </div>
			           <label for="inputEmail3" class="col-sm-3 col-form-label">Edit Password</label>
			           <label for="inputEmail3" class="col-sm-4 col-form-label"><i class="fa fa-info-circle" aria-hidden="true"></i> 6 caracters minimum, letters and/or numbers</label>
			     </div>
			      <div class="form-group row">
			          <label for="inputEmail3" class="col-sm-2 col-form-label">Email Address</label>
			          <div class="col-sm-10">
			              <input type="email" class="form-control" id="inputEmail3" placeholder="Email Address">
			            </div>
			     </div>
			      <div class="form-group row">
			          <label for="inputEmail3" class="col-sm-2 col-form-label">Full Name</label>
			          <div class="col-sm-4">
		                <input type="text" class="form-control" id="inputEmail3" placeholder="First Name">
		               </div>
		               <div class="col-sm-4">
		                <input type="text" class="form-control" id="inputEmail3" placeholder="Last Name">
		               </div>
			     </div>
			      <div class="form-group row">
			          <label for="inputEmail3" class="col-sm-2 col-form-label">Currency</label>
			          <div class="col-sm-5">
			              <select class="form-control" placeholder="Currency">
			                  <option disabled>--Currency--</option>
			              </select>
			            </div>
			     </div>
			      <br/>
			      <!--Notif-->
			      <div class="row">
			          <div class="col-md-12">
			              <p style="font-weight: bold;">Notification Settings</p>
			              <hr style="width: 100%; color: #ffabe1; height: 1px; background-color:#adce74;" />
			          </div>
			      </div>
			      
			     <div class="form-group row">
			          <label for="inputEmail3" class="col-sm-8 col-form-label">Receive transactional e-mail notifications</label>
			          <div class="col-sm-4 text-right">
			              <div class="form-check form-check-inline">
			                  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="Yes" checked>
			                  <label class="form-check-label" for="inlineRadio1">Yes</label>
			             </div>
			             <div class="form-check form-check-inline">
			                 <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="No">
			                 <label class="form-check-label" for="inlineRadio2">No</label>
			             </div>
			          </div>
			     </div>
			     <div class="form-group row">
			          <label for="inputEmail3" class="col-sm-8 col-form-label">Receive a notification when my items arrive at the warehouse</label>
			          <div class="col-sm-4 text-right">
			              <div class="form-check form-check-inline">
			                  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="Yes" checked>
			                  <label class="form-check-label" for="inlineRadio1">Yes</label>
			             </div>
			             <div class="form-check form-check-inline">
			                 <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="No">
			                 <label class="form-check-label" for="inlineRadio2">No</label>
			             </div>
			          </div>
			     </div>
			     <div class="form-group row">
			          <label for="inputEmail3" class="col-sm-8 col-form-label">Receive a notification when my order status changes</label>
			          <div class="col-sm-4 text-right">
			              <div class="form-check form-check-inline">
			                  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="Yes" checked>
			                  <label class="form-check-label" for="inlineRadio1">Yes</label>
			             </div>
			             <div class="form-check form-check-inline">
			                 <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="No">
			                 <label class="form-check-label" for="inlineRadio2">No</label>
			             </div>
			          </div>
			     </div>
			     <div class="form-group row">
			          <label for="inputEmail3" class="col-sm-8 col-form-label">Receive a notification about my buying requests</label>
			          <div class="col-sm-4 text-right">
			              <div class="form-check form-check-inline">
			                  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="Yes" checked>
			                  <label class="form-check-label" for="inlineRadio1">Yes</label>
			             </div>
			             <div class="form-check form-check-inline">
			                 <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="No">
			                 <label class="form-check-label" for="inlineRadio2">No</label>
			             </div>
			          </div>
			     </div>
			     <div class="form-group row">
			          <label for="inputEmail3" class="col-sm-8 col-form-label">Receive a notification when I get an answer from the Customer Centre</label>
			          <div class="col-sm-4 text-right">
			              <div class="form-check form-check-inline">
			                  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="Yes" checked>
			                  <label class="form-check-label" for="inlineRadio1">Yes</label>
			             </div>
			             <div class="form-check form-check-inline">
			                 <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="No">
			                 <label class="form-check-label" for="inlineRadio2">No</label>
			             </div>
			          </div>
			     </div>
			      <br/>
			      <!--Social-->
			      <div class="row">
			          <div class="col-md-12">
			              <p style="font-weight: bold;">Linked social media accounts</p>
			              <hr style="width: 100%; color: #ffabe1; height: 1px; background-color:#adce74;" />
			          </div>
			      </div>
			      <div class="form-group row">
			          <label for="inputEmail3" class="col-sm-2 col-form-label">Google</label>
			          <div class="col-sm-6">
			              <input type="text" class="form-control" id="inputEmail3" >
			            </div>
			          <div class="col-sm-4 text-right">
			              <button class="btn btn-outline-success">Edit</button>
			          </div>
			     </div>
			     <div class="form-group row">
			          <label for="inputEmail3" class="col-sm-2 col-form-label">Twitter</label>
			          <div class="col-sm-6">
			              <input type="text" class="form-control" id="inputEmail3" >
			            </div>
			          <div class="col-sm-4 text-right">
			              <button class="btn btn-outline-success">Edit</button>
			          </div>
			     </div>
			     <div class="form-group row">
			          <label for="inputEmail3" class="col-sm-2 col-form-label">Kakaotalk</label>
			          <div class="col-sm-6">
			              <input type="text" class="form-control" id="inputEmail3" >
			            </div>
			          <div class="col-sm-4 text-right">
			              <button class="btn btn-outline-success">Edit</button>
			          </div>
			     </div>
			     <div class="form-group row">
			          <label for="inputEmail3" class="col-sm-2 col-form-label">Line</label>
			          <div class="col-sm-6">
			              <input type="text" class="form-control" id="inputEmail3" >
			            </div>
			          <div class="col-sm-4 text-right">
			              <button class="btn btn-outline-success">Edit</button>
			          </div>
			     </div>
			     <div class="form-group row">
			          <div class="col-sm-12 text-right">
			              <button class="btn btn-success">SAVE CHANGES</button>
			          </div>
			     </div>
		  		</div>
    		</div>
    		<?php include('paging.php');?>
    	</div>
    </section>
</main>
<?php include('footer.php');?>
<?php include('footer_end.php');?>