<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>SUNBAE</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Kelly - v2.1.1
  * Template URL: https://bootstrapmade.com/kelly-free-bootstrap-cv-resume-html-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
  <style type="text/css">
    .track {
         position: relative;
         background-color: #ddd;
         height: 7px;
         display: -webkit-box;
         display: -ms-flexbox;
         display: flex;
         margin-bottom: 40px;
         margin-top: 20px
     }
    
     .track .step {
         -webkit-box-flex: 1;
         -ms-flex-positive: 1;
         flex-grow: 1;
         width: 25%;
         margin-top: -18px;
         text-align: center;
         position: relative
     }
    
     .track .step.active:before {
         background: #FF5722
     }
    
     .track .step::before {
         height: 7px;
         position: absolute;
         content: "";
         width: 100%;
         left: 0;
         top: 18px
     }
    
     .track .step.active .icon {
         background: #ee5435;
         color: #fff
     }
    
     .track .icon {
         display: inline-block;
         width: 40px;
         height: 40px;
         line-height: 40px;
         position: relative;
         border-radius: 100%;
         background: #ddd
     }
     .track .icon i {
         margin-top:15px;
     }
    
     .track .step.active .text {
         font-weight: 400;
         color: #000
     }
    
     .track .text {
         display: block;
         margin-top: 7px
     }
  </style>
</head>

<body>