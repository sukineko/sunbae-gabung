<?php include('header.php'); include('nav.php'); ?>
<main id="main">
	<section id="resume" class="resume" style="background-color:#dddddd; ">
    	<div class="container" data-aos="fade-up">
    		<div class="section-title" style="margin-top:100px;">
    			<div class="row">
	    			<div class="col-md-4">
	    				<img src="assets/img/maskot/WorkPanda.png" height="100px" width="100px"/>
	    			</div>
	    			<div class="col-md-8" >
	    				<h1 class="jumbotron-heading" style="display:inline;font-weight: bold;">SHIPPING CALCULATOR</h1>
	    			</div>
	    		</div>
        	</div>
        </div>
    </section>
    <section id="services" class="services">
    	<div class="container" data-aos="fade-up">
    		
            <<div class="row">

		  <div class="col-md-4 text-center">
			<select class="form-control">
			    <option>--SELECT WEIGHT--</option>
			</select>
		  </div>
		  <div class="col-md-4">
			<select class="form-control">
			    <option>--SELECT COUNTRY--</option>
			</select>
		  </div>
		  <div class="col-md-4">
			<input class="form-control" placeholder="length (cm)" type="text"/>
		  </div>
		  
		</div>

		<div class="row  text-right ">
		    <div class="col-md-4">
		        
		    </div>
		    <div class="col-md-4">
		        
		    </div>
		    <div class="col-md-4">
		        <input class="form-control" placeholder="width (cm)" type="text"/>
		    </div>
		</div>
		<div class="row ">
		    <div class="col-md-4">
		        
		    </div>
		    <div class="col-md-4">
		        
		    </div>
		  <div class="col-md-4">
		        <input class="form-control" placeholder="height (cm)" type="text"/>
		    </div>
		</div>
		<br/>
		<div class="row ">
		    <div class="col-md-12 text-right">
		        <button class="btn  btn-sm" style="background-color:#fcd1d1;border-radius: 20%;">CALCULATE RATE</button>
		    </div>
		</div>
		
		<hr style="width: 100%; color: #ffabe1; height: 1px; background-color:#ffabe1;" />
		
		<br/>
		<div class="row text-center">
		    <div class="col-md-3">
		        <div class="card" style="background-color:#ffabe1;">
		            <div class="card-body">
		                <b>EXPRESS SHIPPING</b><br/>
		                <b>VIA AIR</b><br/><br/>
		                <b>FEDEX</b><br/>
		                2-4 Days<br/>
		                Max 50kg<br/>
		                <button class="btn btn-outline-danger btn-sm">$55,67</button>
		            </div>
		        </div>
		    </div>
		    <div class="col-md-3">
		        <div class="card" style="background-color:#ffabe1;">
		            <div class="card-body">
		                <b>EXPRESS SHIPPING</b><br/>
		                <b>VIA AIR</b><br/><br/>
		                <b>EMS</b><br/>
		                2-4 Days<br/>
		                Max 50kg<br/>
		                <button class="btn btn-outline-danger btn-sm">$55,67</button>
		            </div>
		        </div>
		    </div>
		    <div class="col-md-3">
		        <div class="card" style="background-color:#ffabe1;">
		            <div class="card-body">
		                <b>EXPRESS SHIPPING</b><br/>
		                <b>VIA AIR</b><br/><br/>
		                <b>DHL</b><br/>
		                2-4 Days<br/>
		                Max 50kg<br/>
		                <button class="btn btn-outline-danger btn-sm">$55,67</button>
		            </div>
		        </div>
		    </div>
		    <div class="col-md-3">
		        <div class="card" style="background-color:#ffabe1;">
		            <div class="card-body">
		                <b>EXPRESS SHIPPING</b><br/>
		                <b>VIA AIR</b><br/><br/>
		                <b>EMS</b><br/>
		                2-4 Days<br/>
		                Max 50kg<br/>
		                <button class="btn btn-outline-danger btn-sm">$55,67</button>
		            </div>
		        </div>
		    </div>
		</div>
		<br/>
		<div class="row text-center">
		    <div class="col-md-3">
		        <div class="card" style="background-color:#ffabe1;">
		            <div class="card-body">
		                <b>EXPRESS SHIPPING</b><br/>
		                <b>VIA AIR</b><br/><br/>
		                <b>AIRMAIL</b><br/>
		                7-28 Days<br/>
		                Max 30kg<br/>
		                <button class="btn btn-outline-danger btn-sm">$55,67</button>
		            </div>
		        </div>
		    </div>
		    <div class="col-md-3">
		        <div class="card" style="background-color:#ffabe1;">
		            <div class="card-body">
		                <b>EXPRESS SHIPPING</b><br/>
		                <b>VIA AIR</b><br/><br/>
		                <b>K-PACKET</b><br/>
		                7-28 Days<br/>
		                Max 2kg<br/>
		                <button class="btn btn-outline-danger btn-sm">$55,67</button>
		            </div>
		        </div>
		    </div>
		    <div class="col-md-3">
		        <div class="card" style="background-color:#ffabe1;">
		            <div class="card-body">
		                <b>EXPRESS SHIPPING</b><br/>
		                <b>VIA AIR</b><br/><br/>
		                <b>SAL</b><br/>
		                2-3 Months<br/>
		                Max 50kg<br/>
		                <button class="btn btn-outline-danger btn-sm">$55,67</button>
		            </div>
		        </div>
		    </div>
		    <div class="col-md-3">
		        <div class="card" style="background-color:#ffabe1; opacity:0.3;">
		            <div class="card-body">
		                <b>EXPRESS SHIPPING</b><br/>
		                <b>VIA AIR</b><br/><br/>
		                <b>LBC EXPRESS</b><br/>
		                5-7 Days<br/>
		                Max 80kg<br/>
		                Philipines Only<br/>
		                <button class="btn btn-outline-danger btn-sm">NOT AVAILABLE</button>
		            </div>
		        </div>
		    </div>
		</div>
    	</div>
    </section>
</main>
<?php include('footer.php');?>
<?php include('footer_end.php');?>