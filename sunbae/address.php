<?php include('header.php'); include('nav.php'); ?>
<main id="main">
	<section id="resume" class="resume" style="background-color:#ffb4b4; ">
    	<div class="container" data-aos="fade-up">
    		<div class="section-title" style="margin-top:50px;">
    			<div class="row">
	    			<div class="col-md-4">
	    				<img src="assets/img/maskot/WorkPanda.png" height="150px" width="150px"/>
	    			</div>
	    			<div class="col-md-8" style="margin-top:50px; margin-left:-15%;">
	    				<h1 class="jumbotron-heading" style="display:inline;font-weight: bold;">ADDRESS BOOK</h1>
	    			</div>
	    		</div>
        	</div>
        </div>
    </section>
    <section id="services" class="services">
    	<div class="container" data-aos="fade-up">
    		
            <div class="row">
    			<div class="col-md-3">
    				<div class="col-12">
    					<?php include('sidebar.php');?>
			        </div>
    			</div>
    			<div class="col-md-9">
    				<div class="row text-center">
    					<div class="col-md-12">
    						<h3>KOREAN ADDRESS</h3>
    					</div>
    					<div class="col-md-12" style="font-size:10pt;">
    						<p>Here is your very own korean address that you can use when you shop alone and wish to ship your items to iur warehouse.
    						Please make sure to use the same name as the one you registered as without forgetting your account ID number. That is how
    						we identify you with the others. If you forget the ID number, we cannot identify you.</p>
    					</div>
					</div>
					<div class="row form-group">
						<div class="col-md-4" style="margin-left:10%;">
							<b>Account Id</b>
						</div>
						<div class="col-md-6">
							#ID00368
						</div>
						<hr style="width: 100%; color: #ffabe1; margin-top:-2px; margin-bottom:-2px; height: 1px; background-color:#ffabe1;" />
						
						<div class="col-md-4" style="margin-left:10%;">
						    <b>Name</b>
						</div>
						<div class="col-md-6">
						    Karima DABO
						</div>
						<hr style="width: 100%; color: #ffabe1; margin-top:-2px; margin-bottom:-2px; height: 1px; background-color:#ffabe1;" />
						
						<div class="col-md-4" style="margin-left:10%;">
						    <b>Address</b>
						</div>
						<div class="col-md-6">
						    서울시 강남구 역삼동 837-26 (837-26 Yeoksam-dong, Gangnam-gu )
						</div>
						<hr style="width: 100%; color: #ffabe1; margin-top:-2px; margin-bottom:-2px; height: 1px; background-color:#ffabe1;" />
						
						<div class="col-md-4" style="margin-left:10%;">
						    <b>Building, Unit, etc</b>
						</div>
						<div class="col-md-6">
						    삼일프라자 819호 (Samil Plaza Building, Unit 819, 8th Floor)
						</div>
						<hr style="width: 100%; color: #ffabe1; margin-top:-2px; margin-bottom:-2px; height: 1px; background-color:#ffabe1;" />
						
						<div class="col-md-4" style="margin-left:10%;">
						    <b>Zip Code</b>
						</div>
						<div class="col-md-6">
						    06253 (Seoul)
						</div>
						<hr style="width: 100%; color: #ffabe1; margin-top:-2px; margin-bottom:-2px; height: 1px; background-color:#ffabe1;" />
						
						<div class="col-md-4" style="margin-left:10%;">
						    <b>Phone Number</b>
						</div>
						<div class="col-md-6">
						    010-3082-5888
						</div>
						<hr style="width: 100%; color: #ffabe1; margin-top:-2px; margin-bottom:-2px; height: 1px; background-color:#ffabe1;" />
						
						<div class="col-md-4" style="margin-left:10%;">
						    <b>Full address</b>
						</div>
						<div class="col-md-6">
						    서울시 강남구 역삼동 837-26 (06253)<br/>
						    삼일프라자, 819호 #ID00368<br/>
						    Karima D<br/>
						    010-3082-5888
						</div>
						<hr style="width: 100%; color: #ffabe1; margin-top:-2px; margin-bottom:-2px; height: 1px; background-color:#ffabe1;" />
					</div>
					<div class="row">
					    <div class="col-md-12 text-center">
						    <h5>MY MAIN ADDRESSES</h5>
						</div>
					</div>
					<div class="row" style="background-color:#ffabe1; padding:10px;">
					    <div class="col-md-9">
					        <b>ADDRESS 1: HOUSE, PARIS 75008, FRANCE</b>
					    </div>
					    <div class="col-md-3 text-right">
					        <button class="btn btn-outline-secondary btn-sm">Delete</button>
					    </div>
					</div>
					<div class="row" style="background-color:#ffabe1; padding:10px; margin-top:10px;">
					    <div class="col-md-9">
					        <b>ADDRESS 2: JAPAN WAREHOUSE, TOKYO, 150-010, JAPAN</b>
					    </div>
					    <div class="col-md-3 text-right">
					        <button class="btn btn-outline-secondary btn-sm">Delete</button>
					    </div>
					</div>
					<div class="row" style="background-color:#ffabe1; padding:10px;  margin-top:10px;">
					    <div class="col-md-9">
					        <b>ADDRESS 3: NO ADDRESS</b>
					    </div>
					    <div class="col-md-3 text-right">
					        <button class="btn btn-outline-secondary btn-sm">Delete</button>
					    </div>
					</div>
					<br/>
					<div class="row">
					    <div class="col-md-4">
					        <input type="text" class="form-control" placeholder="Adress line 1 (Street) " />
					    </div>
					    <div class="col-md-4">
					        <input type="text" class="form-control" placeholder="Adress line 2 (Building, Floor,etc) " />
					    </div>
					    <div class="col-md-4">
					        <input type="text" class="form-control" placeholder="Full Name" />
					    </div>
					    <hr/>
					    <div class="col-md-4">
					        <input type="text" class="form-control" placeholder="Zip Code" />
					    </div>
					    <div class="col-md-4">
					        <input type="text" class="form-control" placeholder="City" />
					    </div>
					    <div class="col-md-4">
					        <input type="text" class="form-control" placeholder="Phone Number" />
					    </div>
					    
					    <div class="col-md-4">
					        <input type="text" class="form-control"  placeholder="State" />
					    </div>
					    <div class="col-md-4">
					        <input type="text" class="form-control"  placeholder="Country" />
					    </div>
					    <div class="col-md-4">
					        <input type="text" class="form-control"  placeholder="Address Name (‘My house‘)" />
					    </div>
					</div>
					<br/>
					<div class="row">
					    <div class="col-md-12 text-right">
					        <button class="btn btn-outline-danger btn-sm">ADD</button>
					    </div>
					</div>
		  		</div>
    		</div>
    		<?php include('paging.php');?>
    	</div>
    </section>
</main>
<?php include('footer.php');?>
<?php include('footer_end.php');?>