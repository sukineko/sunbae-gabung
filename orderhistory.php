<?php include('header.php'); include('nav.php'); ?>
<main id="main">
	<section id="resume" class="resume" style="background-color:#ffb4b4; ">
    	<div class="container" data-aos="fade-up">
    		<div class="section-title" style="margin-top:50px;">
    			<div class="row">
	    			<div class="col-md-4">
	    				<img src="assets/img/maskot/Shoppingpanda.png" height="100px" width="100px"/>
	    			</div>
	    			<div class="col-md-8" style="margin-top:50px; margin-left:-15%;">
	    				<h1 class="jumbotron-heading" style="display:inline;font-weight: bold;">ORDER HISTORY</h1>
	    			</div>
	    		</div>
        	</div>
        </div>
    </section>
    <section id="services" class="services">
    	<div class="container" data-aos="fade-up">
    		
            <div class="row">
    			<div class="col-md-3">
    				<div class="col-12">
    					<?php include('sidebar.php');?>
			        </div>
    			</div>
    			<div class="col-md-9">
    				<div class="row text-center" style="font-size:10pt;">
				        <div class="col-md-12">
		        			<div class="card mb-4 box-shadow">
		        			  <div class="card-body">
		        			      <div class="row">
		            				<div class="col-md-1">
		            				    <img class="card-img-top" src="assets/img/maskot/WorkPanda.png" height="40px" width="30px"/>
		            				</div>
		            				<div class="col-md-3 text-left">
		            				    <b style="color:#7fdbda">ORDER NUMBER</b><br/>
		                                <b>N°SM138448</b>
		            				</div>
		            				<div class="col-md-3">
		            				    <b style="color:#7fdbda">DATE OF PURCHASE</b><br/>
		                                <b>2020/07/14</b>
		            				</div>
		            				<div class="col-md-2">
		            				    <b style="color:#7fdbda">TOTAL PRICE</b><br/>
		                                <b>130,57USD</b>
		            				</div>
		            				<div class="col-md-2">
		            				    <button class="btn btn-outline-danger btn-sm">CHECK DETAILS</button>
		            				</div>
		            				<div class="col-md-1">
		            				    <i class="fa fa-angle-right fa-lg"></i>
		            				</div>
		        				</div>
		        			  </div>
		        			</div>
		    			</div>
		    			<div class="col-md-12">
		        			<div class="card mb-4 box-shadow">
		        			  <div class="card-body">
		        			      <div class="row">
		            				<div class="track col-md-12">
		                                <div class="step active"> <span class="icon"> <i class="icofont-check"></i> </span> <span class="text"> Payment Completed</span> </div>
		                                <div class="step active"> <span class="icon"> <i class="icofont-ui-user"></i> </span> <span class="text"> Processing Order</span> </div>
		                                <div class="step"> <span class="icon"> <i class="icofont-box"></i> </span> <span class="text">Arrived to Warehouse</span> </div>
		                                <div class="step"> <span class="icon"> <i class="icofont-truck-alt"></i> </span> <span class="text"> Shipping Request </span> </div>
		                                <div class="step"> <span class="icon"> <i class="icofont-truck-alt"></i> </span> <span class="text"> Ready To Ship </span> </div>
		                                <div class="step"> <span class="icon"> <i class="icofont-ship"></i> </span> <span class="text"> Shipped Out </span> </div>
		                            </div>
		        				</div>
		        			  </div>
		        			</div>
		    			</div>
		    			<div class="col-md-12">
		        			<div class="card mb-4 box-shadow">
		        			  <div class="card-body">
		        			      <div class="row">
		            				<div class="col-md-1">
		            				    <img class="card-img-top" src="assets/img/maskot/WorkPanda.png" height="40px" width="30px"/>
		            				</div>
		            				<div class="col-md-3 text-left">
		            				    <b style="color:#7fdbda">ORDER NUMBER</b><br/>
		                                <b>N°SM138448</b>
		            				</div>
		            				<div class="col-md-3">
		            				    <b style="color:#7fdbda">DATE OF PURCHASE</b><br/>
		                                <b>2020/07/14</b>
		            				</div>
		            				<div class="col-md-2">
		            				    <b style="color:#7fdbda">TOTAL PRICE</b><br/>
		                                <b>130,57USD</b>
		            				</div>
		            				<div class="col-md-2">
		            				    <button class="btn btn-outline-danger btn-sm">CHECK DETAILS</button>
		            				</div>
		            				<div class="col-md-1">
		            				    <i class="fa fa-angle-right fa-lg"></i>
		            				</div>
		        				</div>
		        			  </div>
		        			</div>
		    			</div>
		    			<div class="col-md-12">
		        			<div class="card mb-4 box-shadow">
		        			  <div class="card-body">
		        			      <div class="row">
		            				<div class="col-md-1">
		            				    <img class="card-img-top" src="assets/img/maskot/WorkPanda.png" height="40px" width="30px"/>
		            				</div>
		            				<div class="col-md-3 text-left">
		            				    <b style="color:#7fdbda">ORDER NUMBER</b><br/>
		                                <b>N°SM138448</b>
		            				</div>
		            				<div class="col-md-3">
		            				    <b style="color:#7fdbda">DATE OF PURCHASE</b><br/>
		                                <b>2020/07/14</b>
		            				</div>
		            				<div class="col-md-2">
		            				    <b style="color:#7fdbda">TOTAL PRICE</b><br/>
		                                <b>130,57USD</b>
		            				</div>
		            				<div class="col-md-2">
		            				    <button class="btn btn-outline-danger btn-sm">CHECK DETAILS</button>
		            				</div>
		            				<div class="col-md-1">
		            				    <i class="fa fa-angle-right fa-lg"></i>
		            				</div>
		        				</div>
		        			  </div>
		        			</div>
		    			</div>
		    			<div class="col-md-12">
		    			<?php
		                    include('paging.php');
		                ?>
		                </div>
		    		</div>
    			</div>
    		</div>
    		<?php include('paging.php');?>
    	</div>
    </section>
</main>
<?php include('footer.php');?>
<?php include('footer_end.php');?>