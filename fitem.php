<?php include('header.php'); include('nav.php'); ?>
<main id="main">
	<section id="resume" class="resume" style="background-color:#7fdbda; ">
    	<div class="container" data-aos="fade-up">
    		<div class="section-title" style="margin-top:100px;">
    			<div class="row">
	    			<div class="col-md-4">
	    				<img src="assets/img/maskot/WorkPanda.png" height="100px" width="100px"/>
	    			</div>
	    			<div class="col-md-8" >
	    				<h1 class="jumbotron-heading" style="display:inline;font-weight: bold;">FOWARD ITEMS</h1>
	    			</div>
	    		</div>
        	</div>
        </div>
    </section>
    <section id="services" class="services">
    	<div class="container" data-aos="fade-up">
    		
            <div class="row">
    			<div class="col-md-3">
    				<div class="col-12">
    					<?php include('sidebar.php');?>
			        </div>
    			</div>
    			<div class="col-md-9">
			         <div class="input-group mb-3">
		              <input type="text" class="form-control" placeholder="Find ...." aria-label="Find ...." aria-describedby="basic-addon2">
		              <div class="input-group-append">
		                <button class="btn btn-outline-secondary" type="button">Find</button>
		              </div>
		            </div>
				      
				    <div class="row text-center" style="font-size:10pt;">
				        <div class="col-md-12">
		        			<div class="card mb-4 box-shadow">
		        			  <div class="card-body">
		        			      <div class="row">
		            				<div class="col-md-2">
		            				    <img class="card-img-top" src="assets/img/maskot/WorkPanda.png" height="70px" width="30px"/>
		            				</div>
		            				<div class="col-md-3 text-left">
		            				    <b style="color:#7fdbda">TRACKING NUMBER</b><br/>
		                                <b>N°SM138448</b><br/>
		                                SMTOWNANDSTORE
		            				</div>
		            				<div class="col-md-3">
		            				    <b style="color:#7fdbda">ITEMS & CATEGORY</b><br/>
		                                <b>FOOD & BEVERAGES</b>
		            				</div>
		            				<div class="col-md-2">
		            				    <b style="color:#7fdbda">STORAGE</b><br/>
		                                <b>13 DAYS</b>
		            				</div>
		            				<div class="col-md-2">
		            				    <b style="color:#7fdbda">WEIGHT</b><br/>
		                                <b>0.850KG</b>
		            				</div>
		        				</div>
		        			  </div>
		        			</div>
		    			</div>
		    			<div class="col-md-12">
		        			<div class="card mb-4 box-shadow">
		        			  <div class="card-body">
		        			      <div class="row">
		            				<div class="col-md-2">
		            				    <img class="card-img-top" src="assets/img/maskot/WorkPanda.png" height="70px" width="30px"/>
		            				</div>
		            				<div class="col-md-3 text-left">
		            				    <b style="color:#7fdbda">TRACKING NUMBER</b><br/>
		                                <b>N°SM138448</b><br/>
		                                SMTOWNANDSTORE
		            				</div>
		            				<div class="col-md-3">
		            				    <b style="color:#7fdbda">ITEMS & CATEGORY</b><br/>
		                                <b>FOOD & BEVERAGES</b>
		            				</div>
		            				<div class="col-md-2">
		            				    <b style="color:#7fdbda">STORAGE</b><br/>
		                                <b>13 DAYS</b>
		            				</div>
		            				<div class="col-md-2">
		            				    <b style="color:#7fdbda">WEIGHT</b><br/>
		                                <b>0.850KG</b>
		            				</div>
		        				</div>
		        			  </div>
		        			</div>
		    			</div>
		    			<div class="col-md-12">
		        			<div class="card mb-4 box-shadow">
		        			  <div class="card-body">
		        			      <div class="row">
		            				<div class="col-md-2">
		            				    <img class="card-img-top" src="assets/img/maskot/WorkPanda.png" height="70px" width="30px"/>
		            				</div>
		            				<div class="col-md-3 text-left">
		            				    <b style="color:#7fdbda">TRACKING NUMBER</b><br/>
		                                <b>N°SM138448</b><br/>
		                                SMTOWNANDSTORE
		            				</div>
		            				<div class="col-md-3">
		            				    <b style="color:#7fdbda">ITEMS & CATEGORY</b><br/>
		                                <b>FOOD & BEVERAGES</b>
		            				</div>
		            				<div class="col-md-2">
		            				    <b style="color:#7fdbda">STORAGE</b><br/>
		                                <b>13 DAYS</b>
		            				</div>
		            				<div class="col-md-2">
		            				    <b style="color:#7fdbda">WEIGHT</b><br/>
		                                <b>0.850KG</b>
		            				</div>
		        				</div>
		        			  </div>
		        			</div>
		    			</div>
		    			<div class="col-md-12">
		    			<?php
		                    include('paging.php');
		                ?>
		                </div>
		    		</div>
    			</div>
    		</div>
    	</div>
    </section>
</main>
<?php include('footer.php');?>
<?php include('footer_end.php');?>