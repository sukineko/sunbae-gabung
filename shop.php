<?php include('header.php'); include('nav.php'); ?>
<main id="main">
	<section id="resume" class="resume" style="background-image: linear-gradient(to right, #e6b3cc , #f2d9e6);">
    	<div class="container" data-aos="fade-up">
    		<div class="section-title">
    			<h2>PRODUCT LIST</h2>
          		<p>Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un peintre anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte...</p>
        	</div>
        </div>
    </section>
    <section id="services" class="services">
    	<div class="container" data-aos="fade-up">
    		<div class="row">
    			<nav aria-label="breadcrumb">
    				<ol class="breadcrumb">
    					<li class="breadcrumb-item"><a href="index.php">Home</a></li>
                    	<li class="breadcrumb-item"><a href="#">Category</a></li>
                    	<li class="breadcrumb-item active" aria-current="page">Sub-category</li>
                	</ol>
            	</nav>
            </div>
            <div class="row">
    			<div class="col-md-3">
    				<div class="col-12">
    					<div class="card bg-light mb-3">
    						<div class="card-header bg-primary text-white text-uppercase"><i class="icofont-listine-dots"></i> Categories</div>
    						<ul class="list-group category_block">
    							<li class="list-group-item"><a href="#">LABEL</a></li>
    							<li class="list-group-item"><a href="#">CD/USB</a></li>
    							<li class="list-group-item"><a href="#">T-SHIRT</a></li>
                    			<li class="list-group-item"><a href="#">HOODIE</a></li>
                    			<li class="list-group-item"><a href="#">SHOES</a></li>
			                </ul>
			            </div>

			            <div class="card bg-light mb-3">
			            	<div class="card-header bg-success text-white text-uppercase">Last product</div>
			            	<div class="card-body">
			            		<img class="img-fluid" src="assets/img/ff.jpg" />
			            		<h5 class="card-title">Product title</h5>
			            		<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
			            		<b style="color:green;">99.00 $</b>
			            	</div>
			            </div>
			        </div>
    			</div>
    			<div class="col-md-9">
    				<div class="row">
    					<div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
		    				<div class="card mb-4">
		    					<img class="card-img-top" src="assets/img/ff.jpg" alt="Card image cap">
		    					<div class="card-body">
		    						<h4 class="card-title"><a href="produk.php" title="View Product">Product title</a></h4>
		    						<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
		    						<div class="row">
		    							<div class="col">
		    								<b style="color:green;">99.00 $</b>
		    							</div>
		    							<div class="col">
		    								<a class="btn btn-success btn-sm btn-block btn-add-cart">Add to cart <span class="badge badge-secondary badge-pill"></span></a>
		    							</div>
		    						</div>
		    					</div>
		    				</div>
		    			</div>
		    			<div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
		    				<div class="card mb-4">
		    					<img class="card-img-top" src="assets/img/ff.jpg" alt="Card image cap">
		    					<div class="card-body">
		    						<h4 class="card-title"><a href="produk.php" title="View Product">Product title</a></h4>
		    						<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
		    						<div class="row">
		    							<div class="col">
		    								<b style="color:green;">99.00 $</b>
		    							</div>
		    							<div class="col">
		    								<a class="btn btn-success btn-sm btn-block btn-add-cart">Add to cart <span class="badge badge-secondary badge-pill"></span></a>
		    							</div>
		    						</div>
		    					</div>
		    				</div>
		    			</div>
		    			<div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
		    				<div class="card mb-4">
		    					<img class="card-img-top" src="assets/img/ff.jpg" alt="Card image cap">
		    					<div class="card-body">
		    						<h4 class="card-title"><a href="produk.php" title="View Product">Product title</a></h4>
		    						<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
		    						<div class="row">
		    							<div class="col">
		    								<b style="color:green;">99.00 $</b>
		    							</div>
		    							<div class="col">
		    								<a class="btn btn-success btn-sm btn-block btn-add-cart">Add to cart <span class="badge badge-secondary badge-pill"></span></a>
		    							</div>
		    						</div>
		    					</div>
		    				</div>
		    			</div>
		    			<div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
		    				<div class="card mb-4">
		    					<img class="card-img-top" src="assets/img/ff.jpg" alt="Card image cap">
		    					<div class="card-body">
		    						<h4 class="card-title"><a href="produk.php" title="View Product">Product title</a></h4>
		    						<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
		    						<div class="row">
		    							<div class="col">
		    								<b style="color:green;">99.00 $</b>
		    							</div>
		    							<div class="col">
		    								<a class="btn btn-success btn-sm btn-block btn-add-cart">Add to cart <span class="badge badge-secondary badge-pill"></span></a>
		    							</div>
		    						</div>
		    					</div>
		    				</div>
		    			</div>
		    			<div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
		    				<div class="card mb-4">
		    					<img class="card-img-top" src="assets/img/ff.jpg" alt="Card image cap">
		    					<div class="card-body">
		    						<h4 class="card-title"><a href="produk.php" title="View Product">Product title</a></h4>
		    						<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
		    						<div class="row">
		    							<div class="col">
		    								<b style="color:green;">99.00 $</b>
		    							</div>
		    							<div class="col">
		    								<a class="btn btn-success btn-sm btn-block btn-add-cart">Add to cart <span class="badge badge-secondary badge-pill"></span></a>
		    							</div>
		    						</div>
		    					</div>
		    				</div>
		    			</div>
		    			<div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
		    				<div class="card mb-4">
		    					<img class="card-img-top" src="assets/img/ff.jpg" alt="Card image cap">
		    					<div class="card-body">
		    						<h4 class="card-title"><a href="produk.php" title="View Product">Product title</a></h4>
		    						<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
		    						<div class="row">
		    							<div class="col">
		    								<b style="color:green;">99.00 $</b>
		    							</div>
		    							<div class="col">
		    								<a class="btn btn-success btn-sm btn-block btn-add-cart">Add to cart <span class="badge badge-secondary badge-pill"></span></a>
		    							</div>
		    						</div>
		    					</div>
		    				</div>
		    			</div>
    				</div>
    			</div>
    		</div>
    		<?php include('paging.php');?>
    	</div>
    </section>
</main>
<?php include('footer.php');?>
<script>      
	var data_cart = [];
	function onLoad(){
		data_cart = JSON.parse(localStorage.getItem('data_cart'));
	}  

    $('.btn-add-cart').click(function(){
        data_cart.push({name:"Product title",price:"99.00"});
        alert("Success To Add");
        console.log(data_cart);
        var current_number = $(this).find('span:first').html();
        if (current_number == "")
            current_number = 1;
        else
            current_number = parseInt(current_number)+1;
        
        $(this).find('span:first').html(current_number);
    });
    $('.btn-checkout-link').click(function(){
        localStorage.setItem("data_cart", JSON.stringify(data_cart));
        window.location.href='checkout.php';
   });
</script>
<?php include('footer_end.php');?>