<?php include('header.php'); include('nav.php'); ?>
<main id="main">
	<section id="resume" class="resume" style="background-color:yellow; ">
    	<div class="container" data-aos="fade-up">
    		<div class="section-title" style="margin-top:100px;">
    			<div class="row">
	    			<div class="col-md-4">
	    				<img src="assets/img/maskot/Shoppingpanda.png" height="100px" width="100px"/>
	    			</div>
	    			<div class="col-md-8" >
	    				<h1 class="jumbotron-heading" style="display:inline;font-weight: bold;">WISHLIST</h1>
	    			</div>
	    		</div>
        	</div>
        </div>
    </section>
    <section id="services" class="services">
    	<div class="container" data-aos="fade-up">
    		
            <div class="row">
    			<div class="col-md-3">
    				<div class="col-12">
    					<?php include('sidebar.php');?>
			        </div>
    			</div>
    			<div class="col-md-9">
    				<div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="Find ...." aria-label="Find ...." aria-describedby="basic-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="button">Find</button>
                    </div>
                </div>
		      
			    <div class="row text-center" style="font-size:10pt;">
    		        <div class="col-md-3">
            			<div class="card mb-4 box-shadow" style="width: 12rem;">
            			    <img class="card-img-top" src="assets/img/2.jpg" alt="Card image cap">
            			    <div class="card-body">
            			      <b>BT21 WASHI TAPE</b><br/>
            			      STUDIO 8<br/>
            			      $2,00
            			      <a href="produk.php"><button class="btn btn-primary">Product Detail</button></a>
            				  <button class="btn btn-danger">Delete</button>
            				</div>
            			  </div>
            			</div>
        			<div class="col-md-3">
        			<div class="card mb-4 box-shadow" style="width: 12rem;">
        			    <img class="card-img-top" src="assets/img/2.jpg" alt="Card image cap">
        			    <div class="card-body">
        			      <b>BT21 WASHI TAPE</b><br/>
        			      STUDIO 8<br/>
        			      $2,00
        			      <a href="produk.php"><button class="btn btn-primary">Product Detail</button></a>
            			  <button class="btn btn-danger">Delete</button>
        				</div>
        			  </div>
        			</div>
        			<div class="col-md-3">
        			<div class="card mb-4 box-shadow" style="width: 12rem;">
        			    <img class="card-img-top" src="assets/img/2.jpg" alt="Card image cap">
        			    <div class="card-body">
        			      <b>BT21 WASHI TAPE</b><br/>
        			      STUDIO 8<br/>
        			      $2,00
        			      <a href="produk.php"><button class="btn btn-primary">Product Detail</button></a>
            				  <button class="btn btn-danger">Delete</button>
        				</div>
        			  </div>
        			</div>
        			<div class="col-md-3">
        			<div class="card mb-4 box-shadow" style="width: 12rem;">
        			    <img class="card-img-top" src="assets/img/2.jpg" alt="Card image cap">
        			    <div class="card-body">
        			      <b>BT21 WASHI TAPE</b><br/>
        			      STUDIO 8<br/>
        			      $2,00
        			      <a href="produk.php"><button class="btn btn-primary">Product Detail</button></a>
            				  <button class="btn btn-danger">Delete</button>
        				</div>
        			  </div>
        			</div>
    			</div>
    			<div class="row">
    			    <div class="col-md-12">
        			<?php
                        include('paging.php');
                    ?>
                    </div>
    			</div>
    			</div>
    		</div>
    	</div>
    </section>
</main>
<?php include('footer.php');?>
<?php include('footer_end.php');?>