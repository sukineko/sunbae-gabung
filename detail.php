<?php include('header.php'); include('nav.php'); ?>
<main id="main">
	<div class="row">
	<div class="col-md-6">
		<section id="testimonials" class="testimonials">
			<div class="container" data-aos="fade-up">
				<div class="owl-carousel testimonials-carousel">
					<div class="testimonial-item">
						<img src="assets/img/slide/1.jpg" alt="">
						<h3>BUYING SERVICES</h3>
						<h4>sunbae shop</h4>
					</div>
					<div class="testimonial-item">
						<img src="assets/img/slide/2.jpg"  alt="">
						<h3>FREE WAREHOUSE</h3>
						<h4>sunbae shop</h4>
					</div>
					<div class="testimonial-item">
						<img src="assets/img/slide/3.jpg" alt="">
						<h3>PACKAGE CONSOLIDATION</h3>
						<h4>sunbae shop</h4>
					</div>
				</div>
			</div>
	    </section>
	</div>
	<div class="col-md-6">
<section id="resume" class="resume" >
    	<div class="container" data-aos="fade-up">
    		<div class="row">
			        <div class="col-md-12">
			            <p>E X O O B S E S S I O N C A S H B E E T R A N S P O R T A T I O N C A R D</p>
			        </div>
			        <div class="col-md-9">
			            <p>S M T O W N A N D S T O R E</p>
			        </div>
			        <div class="col-md-3 text-right">
			            <i class="fa fa-share-alt" aria-hidden="true"></i>
			        </div>
			        <hr style="width: 100%; color: black; height: 1px; background-color:black;" />
			        <div class="col-md-12">
			            <p><b>TAGS :</b> K P O P - E X O - L I F E S T Y L E</p>
			        </div>
			        <hr style="width: 100%; color: black; height: 1px; background-color:black;" />
			        <div class="col-md-12">
			            <b>Required - </b>
			            <select style="width:60%;">
			                <option disabled>Select Version</option>
			            </select>
			        </div>
			        <hr style="width: 100%; color: black; height: 1px; background-color:black;" />
			        <div class="col-md-12">
			            <b>Required - </b>
			            <select style="width:60%;">
			                <option disabled>Select Member</option>
			            </select>
			        </div>
			        <hr style="width: 100%; color: black; height: 1px; background-color:black;" />
			        <div class="col-md-12">
			            <b>Required - </b>
			            <select style="width:60%;">
			                <option disabled>Select Quantity</option>
			            </select>
			        </div>
			        <hr style="width: 100%; color: black; height: 1px; background-color:black;" />
			        
				</div>
        </div>
    </section>
	</div>
	</div>
</main>
<?php include('footer.php');?>

<?php include('footer_end.php');?>