<?php include('header.php'); include('nav.php'); ?>
<main id="main">
	<section id="resume" class="resume" style="background-color:yellow; ">
    	<div class="container" data-aos="fade-up">
    		<div class="section-title" style="margin-top:100px;">
    			<div class="row">
	    			<div class="col-md-4">
	    				<img src="assets/img/maskot/Shoppingpanda.png" height="100px" width="100px"/>
	    			</div>
	    			<div class="col-md-8" >
	    				<h1 class="jumbotron-heading" style="display:inline;font-weight: bold;">REQUEST</h1>
	    			</div>
	    		</div>
        	</div>
        </div>
    </section>
    <section id="services" class="services">
    	<div class="container" data-aos="fade-up">
    		
            <div class="row">
    			<div class="col-md-3">
    				<div class="col-12">
    					<?php include('sidebar.php');?>
			        </div>
    			</div>
    			<div class="col-md-9">
			         <table id="example" class="table table-striped table-bordered" style="width:100%">
		      
				      	  <!-- Button trigger modal -->
		                <button type="button" class="btn btn-primary" >
		                  <a href="buyrequest.php" style="color:white">Buying Request</a>
		                </button>
				      
		                    <thead>
		                        <tr>
		                            <th>No</th>
		                            <th>ID Proses</th>
		                            <th>Name</th>
		                            <th>Description</th>
		                            <th>Unit</th>
		                            <th>Qty</th>
		                            <th>Price</th>
		                            <th>Shipping Price</th>
		                            <th>Url</th>
		                            <th>Photos</th>
		                            <th>Status</th>                
		                            <th>Admin</th>
		                            <th>Action</th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                        <tr>
		                            <td>1</td>
		                            <td>1432535</td>
		                            <td>Label CD</td>
		                            <td>Label for KPOP</td>
		                            <td>Pcs</td>
		                            <td>12</td>
		                            <td>$.0</td>
		                            <td>$.0</td>
		                            <td><button class="btn btn-primary">Url</button></td>
		                            <td><button class="btn btn-success">Images</button></td>
		                            <td><a class=".text-primary">Waiting</label></td>
		                            <td>Waiting Confirm</td>
		                            <td>
		                                <button class="btn btn-warning">Edit</button>
		                                <button class="btn btn-danger">Delete</button>
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>2</td>
		                            <td>454356</td>
		                            <td>Tshirt</td>
		                            <td>Tshirt for KPOP</td>
		                            <td>Pcs</td>
		                            <td>7</td>
		                            <td>$7000</td>
		                            <td>$100</td>
		                            <td><button class="btn btn-primary">Url</button></td>
		                            <td><button class="btn btn-success">Images</button></td>
		                            <td><a class=".text-success">Open</label></td>
		                            <td>Haris.K</td>
		                            <td>
		                                <button class="btn btn-primary">Payment</button>
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>3</td>
		                            <td>45677</td>
		                            <td>Tshirt</td>
		                            <td>Tshirt for KPOP</td>
		                            <td>Pcs</td>
		                            <td>7</td>
		                            <td>$7000</td>
		                            <td>$100</td>
		                            <td><button class="btn btn-primary">Url</button></td>
		                            <td><button class="btn btn-success">Images</button></td>
		                            <td><a class=".text-success">Confirm Payment</label></td>
		                            <td>Haris.K</td>
		                            <td>
		                                --
		                            </td>
		                        </tr>
		                         <tr>
		                            <td>4</td>
		                            <td>65437</td>
		                            <td>Tshirt</td>
		                            <td>Tshirt for KPOP</td>
		                            <td>Pcs</td>
		                            <td>7</td>
		                            <td>$7000</td>
		                            <td>$100</td>
		                            <td><button class="btn btn-primary">Url</button></td>
		                            <td><button class="btn btn-success">Images</button></td>
		                            <td><a class=".text-success">Buying Process</label></td>
		                            <td>Haris.K</td>
		                            <td>
		                                <button class="btn btn-success">Chat</button>
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>5</td>
		                            <td>44536</td>
		                            <td>Tshirt</td>
		                            <td>Tshirt for KPOP</td>
		                            <td>Pcs</td>
		                            <td>7</td>
		                            <td>$7000</td>
		                            <td>$100</td>
		                            <td><button class="btn btn-primary">Url</button></td>
		                            <td><button class="btn btn-success">Images</button></td>
		                            <td><a class=".text-success">Success</label></td>
		                            <td>Haris.K</td>
		                            <td>
		                                <button class="btn btn-success">Package</button>
		                            </td>
		                        </tr>
		                        
		                    </tbody>
		                    <tfoot>
		                        <tr>
		                            <th>No</th>
		                            <th>Name</th>
		                            <th>Description</th>
		                            <th>Unit</th>
		                            <th>Qty</th>
		                            <th>Price</th>
		                            <th>Shipping Price</th>
		                            <th>Url</th>
		                            <th>Photos</th>
		                            <th>Status</th>                
		                            <th>Admin</th>
		                            <th>Action</th>
		                        </tr>
		                    </tfoot>
		                </table>
		                </div>
		    		</div>
    			</div>
    		</div>
    	</div>
    </section>
</main>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Buying Request</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <div>
             <label>Name : </label>
             <input type="text" class="form-control">
        </div>
        
        <div>
             <label>Description : </label>
             <textarea class="form-control"></textarea>
        </div>
        
        <div>
             <label>Unit : </label>
             <input type="text" class="form-control">
        </div>
        
        <div>
             <label>Qty : </label>
             <input type="number" class="form-control">
        </div>
        
        <div>
             <label>Url : </label>
             <input type="text" class="form-control">
        </div>
        
        <div>
             <label>Photos : </label>
             <input type="file" class="form-control">
        </div>
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<?php include('footer.php');?>
<script>
  $(document).ready(function() {
    $('#example').DataTable();
  $('#myModal').on('shown.bs.modal', function () {
      $('#myInput').trigger('focus')
    })
} );
</script>
<?php include('footer_end.php');?>