<header id="header" class="fixed-top" style="color:black;background-color:white;">
	<div class="container-fluid d-flex justify-content-between align-items-center" >
		<nav class="nav-menu d-none d-lg-block" >
			<ul>
				<li class="active"><a href="shop.php">SHOP</a></li>
				<li><a href="#">SERVICES</a></li>
				<li>
					<a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">BUY REQUEST</a>

					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="shop.php">SHOP</a>
      					<a class="dropdown-item" href="#">SERVICES</a>
      					<div class="dropdown-divider"></div>
      					<a class="dropdown-item" href="#">BUYING REQUEST</a>
      					<a class="dropdown-item" href="scal.php">SHIPING CALCULATOR</a>
      				</div>
      			</li>
    		</ul>
    	</nav>

    	<h1><a href="index.php"><img src="assets/img/logo.png" height="50px" width="200px"></a></h1>

    	<nav class="nav-menu d-none d-lg-block">
			<ul>
				<li><a style="color:green;" href="#"><i class="icofont-dollar"></i>/<i class="icofont-won"></i></a></li>
	    		<li><a href="login.php"><i class="icofont-ui-user"></i></a></li>
	    		<li><a href="#"><i class="icofont-ui-search"></i></a></li>
	    		<li class="btn-checkout-link"><a href="#"><i class="icofont-cart"></i></a></li>
	    		<li><a href="#">Payment</a></li>
	    		<!--<li>-->
	    		<!--	<a class="dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Footage</a>-->
	    		<!--	<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">-->
	    		<!--		<a class="dropdown-item" href="#">Action</a>-->
	    		<!--		<a class="dropdown-item" href="#">Another action</a>-->
	    		<!--		<a class="dropdown-item" href="#">Something else here</a>-->
	    		<!--	</div>-->
	    		<!--</li>-->
    		</ul>
    	</nav>
    </div>
</header>